# PLAYTECH TASK

## Description

A small project which extracts weather data from a txt file and displays it in a table with the option to switch between celsius and fahrenheit.

### Stack

Front-end:
- SPA - Angular 7
Git repository: https://gitlab.com/RadoslavDonchev/playtech-task

----------

# Author

Radoslav Donchev