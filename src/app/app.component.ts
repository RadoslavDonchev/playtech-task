import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { DailyWeather } from './contracts/daily-weather';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  constructor(private httpService: HttpClient) { }

  // Variable to keep track of radiobox state.
  option = '1';

  // Array of ojects to be displayed in the table.
  arrDays: DailyWeather[];



  public parsingFunction(inputString: string): DailyWeather[] {

    // Get all the lines from the string into an array.
    const lines = inputString.split('\n');

    // Define an array to hold the DailyWeather objects.
    const result = [];

    // Get the soon to be property names by splitting the first line by whitespaces.
    const headers = lines[0].toLowerCase().trim().split(/[ ]+/);

    // Go over the lines exept the first one, second one(empty),
    // the last one(empty) and the next to last(month total)
    for (let i = 2; i < lines.length - 2; i++) {
      const obj = {};
      const currentline = lines[i].trim().split(/[ ]+/);

      // Go over the first 3 columns only and use the header names to define properties for a new object.
      for (let j = 0; j < 3; j++) {
        obj[headers[j]] = currentline[j];
      }
      result.push(obj);
    }
    return result;
  }



  ngOnInit() {

    // Use the built in method to read the file and receive a string
    // which we parse to an object array and save in arrDays.
    this.httpService.get('assets/weather.txt', { responseType: 'text' }).subscribe(data => {
      this.arrDays = this.parsingFunction(data);
    },
      (err: HttpErrorResponse) => {
        console.log(err.message);
      }
    );

  }



  // Update method to be called whenever the radioboxes' value changes
  // Updates the arrDays variable based on the option variable.
  public updateTemperatures() {
    if (this.option === '1') {
      this.arrDays.map(element => {
        element.mxt = this.convertTemperature(this.convertCelToFa, element.mxt);
        element.mnt = this.convertTemperature(this.convertCelToFa, element.mnt);
        return element;
      });
    }
    if (this.option === '2') {
      this.arrDays.map(element => {
        element.mxt = this.convertTemperature(this.convertFaToCel, element.mxt);
        element.mnt = this.convertTemperature(this.convertFaToCel, element.mnt);
        return element;
      });
    }
  }



  // Two functions which convert a given temperature.
  // If the temperature has an asterisk it is added to the converted one as well.
  convertFaToCel(temp: string): string {
    let resultTemp = Math.round(((parseFloat(temp)) - 32) / 1.8).toString();
    if (temp[temp.length - 1] === '*') {
      resultTemp += '*';
    }
    return resultTemp;
  }

  convertCelToFa(temp: string): string {
    let resultTemp = Math.round(parseFloat(temp) * 1.8 + 32).toString();
    if (temp[temp.length - 1] === '*') {
      resultTemp += '*';
    }
    return resultTemp;
  }



  // Function to take advantage of function passing to allow future extensibility.
  convertTemperature(convertFunc: (temp: string) => any, temp: string): string {
    return convertFunc(temp);
  }

}
